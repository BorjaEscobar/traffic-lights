const {getTrafficHexColor} = require("../src/traffic-light");

testRed = getTrafficHexColor("RED");
console.log(`The return of RED is: ${testRed}`);

testGreen = getTrafficHexColor("GREEN");
console.log(`The return of RED is: ${testGreen}`);

testYELLOW = getTrafficHexColor("YELLOW");
console.log(`The return of RED is: ${testYELLOW}`);

testError = getTrafficHexColor("non-color");
console.log(`The return of RED is: ${testError}`);